
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Picture gallery &mdash; By Kaido, Marta, Sigrid, Kea</title>
    <link rel="stylesheet" href="/account/resources/css/bootstrap.min.css">
    <link rel="stylesheet" href="/account/resources/css/aos.min.css">
    <link rel="stylesheet" href="/account/resources/css/hamburgers/hamburgers.min.css">
    <link rel="stylesheet" href="/account/resources/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/account/resources/fonts/icomoon/style.css">
    <link rel="stylesheet" href="/account/resources/css/animsition.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    
    <link rel="stylesheet" href="/account/resources/css/style.css">
    
    <style>li a, .dropbtn {
  display: inline-block;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}

li.dropdown {
  display: inline-block;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: black;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
  text-align: left;
}

.dropdown-content a:hover {background-color: black;}

.dropdown:hover .dropdown-content {
  display: block;
}
</style>
  </head>
  <body>
  
  <div class="js-animsition animsition" data-animsition-in-class="fade-in" data-animsition-out-class="fade-out">

    <header class="templateux-navbar navbar-light"  data-aos="fade-down">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-3 col-3"><div class="site-logo"><a href="/account/picture/welcome" class="animsition-link">Home</a></div></div>
          <div class="col-sm-9 col-9 text-right">
            <button class="hamburger hamburger--spin toggle-menu ml-auto js-toggle-menu" type="button">
              <span class="hamburger-box">
                <span class="hamburger-inner"></span>
              </span>
            </button>  

            <nav class="templateux-menu js-templateux-menu" role="navigation">
              <ul class="list-unstyled">
 <li class="d-md-none d-block active"><a href="../picture/welcome" class="animsition-link">Home</a></li>
                
                <c:if test="${pageContext.request.userPrincipal.name != null}">
        <form id="logoutForm" method="POST" action="${contextPath}/logout">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </form> <li><a onclick="document.forms['logoutForm'].submit()">Logout</a></li> 
    </c:if>
                 
                      <li class ="dropdown">
                 <a class="dropbtn">View</a>
                 <div class="dropdown-content">
                  <a href="/account/picture/allUploadedPictures" class="animsition-link">All pictures</a>
               <a href="/account/album/allUploadedAlbums" class="animsition-link">All Albums</a>
                </div>
                 </li>
                 
                     <li class ="dropdown">
                 <a class="dropbtn">Add</a>
                 <div class="dropdown-content">
                       <c:if test="${pageContext.request.userPrincipal.name != null}">
        <form id="addalbumForm" method="POST" action="save?${_csrf.parameterName}=${_csrf.token}" enctype="multipart/form-data">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </form>   <a href="/account/album/albumForm" class="animsition-link">Add New Album</a></c:if>
                <c:if test="${pageContext.request.userPrincipal.name != null}">
        <form id="addpictureForm" method="POST" action="save?${_csrf.parameterName}=${_csrf.token}" enctype="multipart/form-data">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </form>    <a href="/account/picture/pictureForm" class="animsition-link">Add New Picture</a>   </c:if>
                </div>
                 </li>
                 
                 <li class ="dropdown">
                 <a class="dropbtn">Edit</a>
                 <div class="dropdown-content">
                   <a href="/account/picture/viewPicture" class="animsition-link">Edit pictures</a>
               <a href="/account/album/albumsview" class="animsition-link">Edit albums</a>
                </div>
                 </li>
              
                   
              </ul>
            </nav>  
          </div>
        </div>
      </div>
    </header>

    
      <!-- END templateux-navbar -->
    <section class="templateux-hero overlay"  data-scrollax-parent="true">
      <div class="cover" data-scrollax="properties: { translateY: '30%' }"><img src="/account/resources/images/taust.jpg"></div>

      <div class="container">
        <div class="row align-items-center justify-content-center intro">
          <div class="col-md-10" data-aos="fade-up">
            <h1>ValiIT Picture Gallery</h1>
            <h1 class = "lead">Welcome ${pageContext.request.userPrincipal.name} </h1>
            <a href="#next" class="go-down js-smoothscroll"></a>
          </div>
        </div>
      </div>
    </section>
    <!-- END templateux-hero -->
    
    <br>
      <br>
       <br>
        <br>
         <br>
          <br>  
         
    <section class="templateux-portfolio-overlap" id="next">
      <div class="container-fluid">
              <div class="row">
          <div class="col-lg-4 col-md-6" data-aos="fade-up">
          <c:forEach var="picture" items="${listFreshestPictures}" begin = '1' end ='1'>
            <a class="project animsition-link" href="getPhoto/<c:out value='${picture.id}'/>">
              <figure>
                <img alt="Free Template" class="img-fluid" src="getPhoto/<c:out value='${picture.id}'/>">
              </figure>
              <div class="project-hover">
                <div class="project-hover-inner">
                <h2>TOP 3 freshest pictures</h2>
                  <h2>Likes: ${picture.likeCount}</h2>
                  <span>Dislikes: ${picture.dislikeCount}</span> <br> </br> 
                  <span>Date added: ${picture.timeAdded}</span><br> </br>
                  <span>Title: ${picture.title}</span> <br> </br>
                  <span>Album: ${picture.albumName}</span>
                </div>
              </div>
            </a>
            </c:forEach>
          </div>
          <div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="100">
            <c:forEach var="picture" items="${listFreshestPictures}" begin = '2' end ='2'>
            <a class="project animsition-link" href="getPhoto/<c:out value='${picture.id}'/>">
              <figure>
                <img alt="Free Template" class="img-fluid" src="getPhoto/<c:out value='${picture.id}'/>">
              </figure>
              <div class="project-hover">
                <div class="project-hover-inner">
                <h2>TOP 3 freshest pictures</h2>
                  <h2>Likes: ${picture.likeCount}</h2>
                  <span>Dislikes: ${picture.dislikeCount}</span> <br> </br> 
                  <span>Date added: ${picture.timeAdded}</span><br> </br>
                  <span>Title: ${picture.title}</span> <br> </br>
                  <span>Album: ${picture.albumName}</span>
                </div>
              </div>
            </a>
            </c:forEach>
          </div>
          <div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="200">
            <c:forEach var="picture" items="${listFreshestPictures}" begin = '3' end ='3'>
             <a class="project animsition-link" href="getPhoto/<c:out value='${picture.id}'/>">
              <figure>
                <img alt="Free Template" class="img-fluid" src="getPhoto/<c:out value='${picture.id}'/>">
              </figure>
              <div class="project-hover">
                <div class="project-hover-inner">
                <h2>TOP 3 freshest pictures</h2>
                  <h2>Likes: ${picture.likeCount}</h2>
                  <span>Dislikes: ${picture.dislikeCount}</span> <br> </br> 
                  <span>Date added: ${picture.timeAdded}</span><br> </br>
                  <span>Title: ${picture.title}</span> <br> </br>
                  <span>Album: ${picture.albumName}</span>
                </div>
              </div>
            </a>
             </c:forEach>
          </div>
        <!-- END row -->
     </div>
      </section>
    <section>

        <div class="row">
          <div class="col-lg-4 col-md-6" data-aos="fade-up">
          <c:forEach var="picture" items="${listMostPopularPictures}" begin = '1' end ='1'>
            <a class="project animsition-link" href="getPhoto/<c:out value='${picture.id}'/>">
              <figure>
                <img alt="Free Template" class="img-fluid" src="getPhoto/<c:out value='${picture.id}'/>">
              </figure>
              <div class="project-hover">
                <div class="project-hover-inner">
                <h2>TOP 3 most popular pictures</h2>
                  <h2>Likes: ${picture.likeCount}</h2>
                  <span>Dislikes: ${picture.dislikeCount}</span> <br> 
                  <span>Date added: ${picture.timeAdded}</span><br> 
                  <span>Title: ${picture.title}</span> <br>
                  <span>Album: ${picture.albumName}</span>
                </div>
              </div>
            </a>
            </c:forEach>
          </div>
          <div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="100">
            <c:forEach var="picture" items="${listMostPopularPictures}" begin = '2' end ='2'>
            <a class="project animsition-link" href="getPhoto/<c:out value='${picture.id}'/>">
              <figure>
                <img alt="Free Template" class="img-fluid" src="getPhoto/<c:out value='${picture.id}'/>">
              </figure>
              <div class="project-hover">
                <div class="project-hover-inner">
                <h2>TOP 3 most popular pictures</h2>
                  <h2>Likes: ${picture.likeCount}</h2>
                  <span>Dislikes: ${picture.dislikeCount}</span> <br> 
                  <span>Date added: ${picture.timeAdded}</span><br>
                  <span>Title: ${picture.title}</span> <br>
                  <span>Album: ${picture.albumName}</span>
                </div>
              </div>
            </a>
            </c:forEach>
          </div>
          <div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="200">
            <c:forEach var="picture" items="${listMostPopularPictures}" begin = '3' end ='3'>
             <a class="project animsition-link" href="getPhoto/<c:out value='${picture.id}'/>">
              <figure>
                <img alt="Free Template" class="img-fluid" src="getPhoto/<c:out value='${picture.id}'/>">
              </figure>
              <div class="project-hover">
                <div class="project-hover-inner">
  					<h2>TOP 3 most popular pictures</h2>
                  <h2>Likes: ${picture.likeCount}</h2>
                  <span>Dislikes: ${picture.dislikeCount}</span> <br>
                  <span>Date added: ${picture.timeAdded}</span><br> 
                  <span>Title: ${picture.title}</span> <br>
                  <span>Album: ${picture.albumName}</span>
                </div>
              </div>
            </a>
             </c:forEach>
          </div>
        
        </div>
        <!-- END row -->
        
        
                <div class="row">
          <div class="col-lg-4 col-md-6" data-aos="fade-up">
          <c:forEach var="albumpicture" items="${listalbums}" begin = '1' end ='1'>
            <a class="project animsition-link" href ="/account/picture/viewAlbums/<c:out value='${albumpicture.albumId}'/>"> 
              <figure>
                <img alt="Free Template" class="img-fluid" src="getPhoto/<c:out value='${albumpicture.pictureId}'/>">
              </figure>
              <div class="project-hover">
                <div class="project-hover-inner">
                  <h2>Album: ${albumpicture.albumName}</h2>
                  <span>Kliki, et vaadata albumis olevaid pilte</span> <br> 
                </div>
              </div>
            </a>
            </c:forEach>
          </div>
          <div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="100">
            <c:forEach var="albumpicture" items="${listalbums}" begin = '4' end ='4'>
            <a class="project animsition-link" href ="/account/picture/viewAlbums/<c:out value='${albumpicture.albumId}'/>">   
              <figure>
                <img alt="Free Template" class="img-fluid" src="../picture/getPhoto/<c:out value='${albumpicture.pictureId}'/>">
              </figure>
              <div class="project-hover">
                <div class="project-hover-inner">
                  <h2>Album: ${albumpicture.albumName}</h2>
                  <span>Kliki, et vaadata albumis olevaid pilte</span> <br> 
                </div>
              </div>
            </a>
            </c:forEach>
          </div>
          <div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="200">
            <c:forEach var="albumpicture" items="${listalbums}" begin = '3' end ='3'>
            <a class="project animsition-link" href ="/account/picture/viewAlbums/<c:out value='${albumpicture.albumId}'/>"> 
              <figure>
                <img alt="Free Template" class="img-fluid" src="../picture/getPhoto/<c:out value='${albumpicture.pictureId}'/>">
              </figure>
              <div class="project-hover">
                <div class="project-hover-inner">
                  <h2>Album: ${albumpicture.albumName}</h2>
                  <span>Kliki, et vaadata albumis olevaid pilte</span> <br> 
                </div>
              </div>
            </a>
            </c:forEach>
          </div>
        
        </div>
        <!-- END row -->
   
   
    </section>
         
    
    <a class="templateux-section templateux-cta animsition-link mt-5" href="pictureForm"" data-aos="fade-up">
      <div class="container-fluid">
        <div class="cta-inner">
          <h2><span class="words-1">Add a new picture yourself.</span> <span class="words-2">It is easy, just click here.</span></h2>
        </div>
      </div>
    </a>  
    <!-- END call to action -->
    
   
    
     <footer class="templateux-footer">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-6 text-md-left text-center">
            <p>
            Copyright &copy;<script>document.write(new Date().getFullYear());</script> </i> <a href="http://vali-it.ee/" target="_blank" class="text-primary">ValiIT</a>
          </p>
          </div>
          <div class="col-md-6 text-md-right text-center footer-social">
                   <a href="/account/picture/allUploadedPictures" class="p-3">All pictures</a>
               <a href="/account/album/allUploadedAlbums" class="p-3">All Albums</a>
          </div>
        </div>
      </div>
    </footer>
           </div>
      <script src="${contextPath}/resources/js/scripts-all.js"></script>
  <script src="${contextPath}/resources/js/main.js"></script>
  
  </body>

</html>
