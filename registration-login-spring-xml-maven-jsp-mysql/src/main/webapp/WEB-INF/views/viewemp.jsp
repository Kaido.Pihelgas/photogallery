    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>  
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  

	<h1>Employees List</h1>
	<table border="2" width="70%" cellpadding="2">
	<tr><th>Id</th><th>Name</th><th>Salary</th><th>Designation</th><th>Edit</th><th>Delete</th><th>Photo</th></tr>
    <c:forEach var="emp" items="${list}"> 
    <tr>
    <td>${emp.id}</td>
    <td>${emp.name}</td>
    <td>${emp.salary}</td>
    <td>${emp.designation}</td>
    
    <td><a href="editemp/${emp.id}">Edit</a></td>
    <td><a href="deleteemp/${emp.id}">Delete</a></td>
    <td><a href="getPhoto/${emp.id}">Photo</a></td>
    </tr>
    </c:forEach>
    </table>
    <br/>
    <!-- valib, mida n�idata adminil if-loop -->
    <c:if test="${isAdmin}">
    <a href="empform">Add New Employee</a>
    </c:if>
    
    <a href="login?logout=true">Log out</a>