package com.hellokoding.account.web;   

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;  
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;  
import org.springframework.web.bind.annotation.PathVariable;  
import org.springframework.web.bind.annotation.RequestMapping;  
import org.springframework.web.bind.annotation.RequestMethod;
import com.hellokoding.account.model.Album;
import com.hellokoding.account.model.AlbumPicture;
import com.hellokoding.account.model.Picture;
import com.hellokoding.account.repository.AlbumDao;
import com.hellokoding.account.repository.PictureDao;


@RequestMapping("album")
@Controller  
public class AlbumController {  	
    	
	@Autowired  
    AlbumDao dao;

    @RequestMapping("/albumForm")  
    public String showformpic(Model m){  
    	m.addAttribute("command", new Album());
    	return "albumForm"; 
    }  
    
    @RequestMapping("/albumsview")  
    public String showalbumview(Model m){  
        List<Album> list = dao.getAlbums();
        m.addAttribute("list",list);
    	return "albumsview"; 
    }   

    @RequestMapping(value="/save",method = RequestMethod.POST)  
    public String save(@ModelAttribute("album") Album album) {
    	dao.save(album);	
    	return "redirect:/welcome";
    }
       
     
    @RequestMapping("/welcome")  
    public String welcome(@PathVariable int id, Model m){  
        List<Album> list = dao.getAlbums();
        m.addAttribute("list",list);
        
        return "welcome";  
    }   
    
    @RequestMapping(value="/editalbum/{id}")  
    public String edit(@PathVariable int id, Model m){  
        Album album=dao.getAlbumById(id);  
        m.addAttribute("command",album);
        return "albumeditform";  
    }  

    @RequestMapping(value="/editsave",method = RequestMethod.POST)  
    public String editsave(@ModelAttribute("album") Album album){  
        dao.update(album);  
        return "redirect:/welcome";  
    }  

    @RequestMapping(value="/deletealbum/{id}",method = RequestMethod.GET)  
    public String delete(@PathVariable int id){  
        dao.delete(id);  
        return "redirect:/welcome";  
    }
    
    @RequestMapping("/allUploadedAlbums")  
    public String allUploadedAlbums(Model m){  
        List<AlbumPicture> list = dao.getAlbumsAndPicture();
        m.addAttribute("list",list);
        return "allUploadedAlbums";  
    } 

}