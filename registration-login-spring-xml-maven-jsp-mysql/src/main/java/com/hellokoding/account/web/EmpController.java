package com.hellokoding.account.web;   
import java.io.ByteArrayInputStream;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;  
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;  
import org.springframework.web.bind.annotation.PathVariable;  
import org.springframework.web.bind.annotation.RequestMapping;  
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.hellokoding.account.model.Department;
import com.hellokoding.account.model.Emp;
import com.hellokoding.account.repository.EmpDao;
import com.hellokoding.account.service.UserService;


@Controller  
public class EmpController {  
	@Autowired
    private UserService userService;
	
    @Autowired  
    EmpDao dao;//will inject dao from xml file  
      
    /*It displays a form to input data, here "command" is a reserved request attribute 
     *which is used to display object data into form 
     */  
    @RequestMapping("/empform")  
    public String showform(Model m){  
    	m.addAttribute("command", new Emp());
    	return "empform"; 
    }  
   
    /*It saves object into database. The @ModelAttribute puts request data 
     *  into model object. You need to mention RequestMethod.POST method  
     *  because default request is GET*/  
    @RequestMapping(value="/save",method = RequestMethod.POST)  
    public String save(@ModelAttribute("emp") Emp emp, 
    		@RequestParam("file") MultipartFile file){
        try {
             emp.setPicture(file.getBytes());
         } catch (IOException e) {
             // TODO Auto-generated catch block
             e.printStackTrace();
         }
        dao.save(emp);
        return "redirect:/viewemp";//will redirect to viewemp request mapping
    } 
  
    /* It provides list of employees in model object */  
    @RequestMapping("/viewemp")  
    public String viewemp(Model m){  
        List<Emp> list=dao.getEmployees();  
        m.addAttribute("list",list);
        m.addAttribute("isAdmin", userService.hasRole("ROLE_ADMIN"));
        return "viewemp";  
    }  
    
    
    /* It displays object data into form for the given id.  
     * The @PathVariable puts URL data into variable.*/  
    @RequestMapping(value="/editemp/{id}")  
    public String edit(@PathVariable int id, Model m){  
        Emp emp=dao.getEmpById(id);  
        m.addAttribute("command",emp);
        return "empeditform";  
    }  
    /* It updates model object. */  
    @RequestMapping(value="/editsave",method = RequestMethod.POST)  
    public String editsave(@ModelAttribute("emp") Emp emp){  
        dao.update(emp);  
        return "redirect:/viewemp";  
    }  
    /* It deletes record for the given id in URL and redirects to /viewemp */  
    @RequestMapping(value="/deleteemp/{id}",method = RequestMethod.GET)  
    public String delete(@PathVariable int id){  
        dao.delete(id);  
        return "redirect:/viewemp";  
    } 
    
    private Map<Integer, String> departmentsToMap(List<Department> departments) {
    	Map<Integer, String> departmentsMap = new HashMap<>();
    	
    	for(Department department : departments ) {
    		departmentsMap.put(department.getId(), department.getName());
    	}
    	
    	return departmentsMap;
    }
    
    @RequestMapping(value = "/getPhoto/{id}")
    public void getPhoto(HttpServletResponse response, @PathVariable("id") int id) throws Exception {
        response.setContentType("image/jpeg");
        
        Emp emp = dao.getEmpById(id);
        byte[] bytes = emp.getPicture();
        InputStream inputStream = new ByteArrayInputStream(bytes);
        IOUtils.copy(inputStream, response.getOutputStream());
    }


}