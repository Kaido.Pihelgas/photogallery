package com.hellokoding.account.web;   
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;  
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;  
import org.springframework.web.bind.annotation.PathVariable;  
import org.springframework.web.bind.annotation.RequestMapping;  
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import com.hellokoding.account.model.Album;
import com.hellokoding.account.model.AlbumPicture;
import com.hellokoding.account.model.Picture;
import com.hellokoding.account.repository.AlbumDao;
import com.hellokoding.account.repository.PictureDao;


@RequestMapping("picture")
@Controller  
public class PictureController {  

	
    @Autowired  
    PictureDao dao;
    @Autowired 
    AlbumDao daoAlbum;


    @RequestMapping("/pictureForm")  
    public String showformpic(Model m){  
    	m.addAttribute("command", new Picture());
    	m.addAttribute("albums", albumsToMap(dao.getAlbumId()));
    	return "pictureForm"; 
    }  

    @RequestMapping(value="/savePic",method = RequestMethod.POST)  
    public String savePic(@ModelAttribute("picture") Picture picture, 
    		@RequestParam("file") MultipartFile file){
        try {
             picture.setPicture(file.getBytes());
         } catch (IOException e) {
             e.printStackTrace();
         }
        dao.savePic(picture);
        return "redirect:/welcome";
    } 
     
    @RequestMapping("/welcome")  
    public String welcome(Model m){  
        List<Picture> list = dao.getPictures();
        m.addAttribute("list",list);
        List<Picture> listFreshestPictures = dao.getFreshestPictures();
        m.addAttribute("listFreshestPictures",listFreshestPictures);
        List<Picture> listMostPopularPictures = dao.getMostPopularPictures();
        m.addAttribute("listMostPopularPictures",listMostPopularPictures);
        List<AlbumPicture> listalbums = daoAlbum.getAlbumsAndPicture();
        m.addAttribute("listalbums",listalbums);
        return "welcome";  
    }   
    
    @RequestMapping("/viewAlbums/{id}")
    public String getPicturesByAlbumId(@PathVariable int id, Model m) throws Exception {
    	
        List<Picture> listPicturesByAlbumId = dao.getPicturesByAlbumId(id);
        m.addAttribute("listPicturesByAlbumId",listPicturesByAlbumId);
        m.addAttribute("albumName", daoAlbum.getAlbumById(id).getTitle());
        m.addAttribute("previousId", daoAlbum.getPreviousAlbumById(id));
        m.addAttribute("nextId", daoAlbum.getNextAlbumById(id));
        m.addAttribute("albumId", id);
 
        return "viewAlbums";
    }
    
    @RequestMapping("/allUploadedPictures")  
    public String allUploadedPictures(Model m){  
        List<Picture> list = dao.getPictures();
        m.addAttribute("list",list);
        List<Picture> listFreshestPictures = dao.getFreshestPictures();
        m.addAttribute("listFreshestPictures",listFreshestPictures);
        List<Picture> listMostPopularPictures = dao.getMostPopularPictures();
        m.addAttribute("listMostPopularPictures",listMostPopularPictures);
        return "allUploadedPictures";  
    }  
 
    @RequestMapping(value = "/getPhoto/{id}")
    public void getPhoto(HttpServletResponse response, @PathVariable("id") int id) throws Exception {
        response.setContentType("image/jpeg");
        
        Picture pic = dao.getPictureById(id);
        byte[] bytes = pic.getPicture();
        InputStream inputStream = new ByteArrayInputStream(bytes);
        IOUtils.copy(inputStream, response.getOutputStream());
    }
    @RequestMapping("/viewPicture")  
    public String viewemp(Model m){  
        List<Picture> list=dao.getPictures();  
        m.addAttribute("list",list);
 
        return "viewPicture";  
    }  

    @RequestMapping(value="/editpicture/{id}")  
    public String edit(@PathVariable int id, Model m){  
        Picture picture=dao.getPictureById(id);  
        m.addAttribute("command",picture);
        m.addAttribute("albums",albumsToMap(dao.getAlbumId()));
        return "pictureeditform";  
    }  

    @RequestMapping(value="/editsave",method = RequestMethod.POST)  
    public String editsave(@ModelAttribute("picture") Picture picture){  
        dao.update(picture);  
        return "redirect:/welcome";  
    }  

    @RequestMapping(value="/deletepicture/{id}",method = RequestMethod.GET)  
    public String delete(@PathVariable int id){  
        dao.delete(id);  
        return "redirect:/welcome";  
    } 
    private Map<Integer, String> albumsToMap(List<Album> albums){
    	Map<Integer, String> albumsMap = new HashMap<>();
    	
    	for(Album album : albums ) {
    		albumsMap.put(album.getId(), album.getTitle());
    	}
    	return albumsMap;
    }


}