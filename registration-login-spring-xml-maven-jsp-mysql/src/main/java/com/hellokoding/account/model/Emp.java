package com.hellokoding.account.model;


public class Emp {  
private int id;  
private String name;  
private float salary;  
private String designation; 
private byte[] picture;
private int pictureId;
  
public int getId() {  
    return id;  
}  
public void setId(int id) {  
    this.id = id;  
}  
public String getName() {  
    return name;  
}  
public void setName(String name) {  
    this.name = name;  
}  
public float getSalary() {  
    return salary;  
}  
public void setSalary(float salary) {  
    this.salary = salary;  
}  
public String getDesignation() {  
    return designation;  
}  
public void setDesignation(String designation) {  
    this.designation = designation;  
}
public byte[] getPicture() {
	return picture;
}
public void setPicture(byte[] picture) {
	this.picture = picture;
}
public int getPictureId() {
	return pictureId;
}
public void setPictureId(int pictureId) {
	this.pictureId = pictureId;
}  
  
}  