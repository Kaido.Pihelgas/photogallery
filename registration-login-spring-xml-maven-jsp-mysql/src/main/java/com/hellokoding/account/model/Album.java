package com.hellokoding.account.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Album {
	
	private String title;
	private int id;
    private List<Album> albums = new ArrayList<Album>();
    private String description;
    private Date albumCreated;
    private Map<String, Integer> likesCounts = new HashMap<String, Integer>();
	

    
 // meetod, mis prindib v�lja k�ik "likes"

    public void printLikesCounts() {

        for (Map.Entry<String, Integer > entry : likesCounts.entrySet()) {
            System.out.printf("Albumil %s on %d likes%n", entry.getKey(), entry.getValue());
        }

    }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<Album> getAlbums() {
		return albums;
	}

	public void setAlbums(List<Album> albums) {
		this.albums = albums;
	}

	public Map<String, Integer> getLikesCounts() {
		return likesCounts;
	}

	public void setLikesCounts(Map<String, Integer> likesCounts) {
		this.likesCounts = likesCounts;
	}

	public Date getAlbumCreated() {
		return albumCreated;
	}

	public void setAlbumCreated(Date albumCreated) {
		this.albumCreated = albumCreated;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	

}