package com.hellokoding.account.model;


public class Picture {   
private String title;  
private String description;
private int id;
private int albumId;
private String albumName;
private String albumDescription;
private byte[] picture;
private String timeAdded;
private int likeCount;
private int dislikeCount;


public String getTitle() {
	return title;
}

public void setTitle(String pictureTitle) {
	this.title = pictureTitle;
}

public String getDescription() {
	return description;
}

public void setDescription(String pictureDescription) {
	this.description = pictureDescription;
}
public void setAlbumId(int albumId) {
	this.albumId = albumId;
}

public String getTimeAdded() {
	return timeAdded;
}

public void setTimeAdded(String timeAdded) {
	this.timeAdded = timeAdded;
}


public int getAlbumId() {
	return albumId;
}


public byte[] getPicture() {
	return picture;
}

public void setPicture(byte[] picture) {
	this.picture = picture;
}

public int getLikeCount() {
	return likeCount;
}

public void setLikeCount(int likeCount) {
	this.likeCount = likeCount;
}

public int getDislikeCount() {
	return dislikeCount;
}

public void setDislikeCount(int dislikeCount) {
	this.dislikeCount = dislikeCount;
}

public String getAlbumName() {
	return albumName;
}

public void setAlbumName(String albumName) {
	this.albumName = albumName;
}

public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}

public String getAlbumDescription() {
	return albumDescription;
}

public void setAlbumDescription(String string) {
	this.albumDescription = string;
	
}

  
} 