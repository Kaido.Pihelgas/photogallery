package com.hellokoding.account.model;


public class Likes {   
private int likeId;
private int pictureId;
private int userId;
private boolean isLike;



public int getLikeId() {
	return likeId;
}
public void setLikeId(int likeId) {
	this.likeId = likeId;
}
public int getPictureId() {
	return pictureId;
}
public void setPictureId(int pictureId) {
	this.pictureId = pictureId;
}
public int getUserId() {
	return userId;
}
public void setUserId(int userId) {
	this.userId = userId;
}
public boolean isLike() {
	return isLike;
}
public void setLike(boolean isLike) {
	this.isLike = isLike;
}


  
} 