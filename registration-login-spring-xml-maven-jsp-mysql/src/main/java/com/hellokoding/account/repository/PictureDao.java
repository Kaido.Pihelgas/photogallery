package com.hellokoding.account.repository;
        import java.io.ByteArrayInputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.sql.Types;


import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.support.SqlLobValue;
import org.springframework.jdbc.support.lob.DefaultLobHandler;

import com.hellokoding.account.model.Album;
import com.hellokoding.account.model.Picture;
public class PictureDao {

	private JdbcTemplate jdbcTemplate;

   	
   	public int savePic(Picture p){
   	   MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
   	   mapSqlParameterSource.addValue("title", p.getTitle());
   	   mapSqlParameterSource.addValue("description", p.getDescription());
   	   mapSqlParameterSource.addValue("picture",  new SqlLobValue(new ByteArrayInputStream(p.getPicture()), 
   	   	      p.getPicture().length, new DefaultLobHandler()), Types.BLOB);
   	      NamedParameterJdbcTemplate jdbcTemplateObject = new
   	                NamedParameterJdbcTemplate(jdbcTemplate.getDataSource());
   	   mapSqlParameterSource.addValue("albumId", p.getAlbumId());
   	mapSqlParameterSource.addValue("likeCount", p.getLikeCount());
   	mapSqlParameterSource.addValue("dislikeCount", p.getDislikeCount());
   	      
   	   String sql = "insert into Pictures(title,description,picture, albumId, likeCount, dislikeCount) "
   	           + "VALUES (:title, :description, :picture, :albumId, :likeCount, :dislikeCount)";

   	  return jdbcTemplateObject.update(sql, mapSqlParameterSource);
   	}
   
    public void setTemplate(JdbcTemplate template) {
        this.jdbcTemplate = template;
    }

    public int update(Picture p){
        String sql="update Pictures set title='"+p.getTitle()+"',description='"+p.getDescription()+"',albumId='"+p.getAlbumId()+"',likeCount='"+p.getLikeCount()+"',dislikeCount='"+p.getDislikeCount()+"' where id="+p.getId()+"";
        return jdbcTemplate.update(sql);
    }
    public int delete(int id){
        String sql="delete from Pictures where id="+id+"";
        return jdbcTemplate.update(sql);
    }
    public Picture getPictureById(int id){
        String sql="select * from Pictures where id=?";
        return jdbcTemplate.queryForObject(sql, new Object[]{id},new BeanPropertyRowMapper<Picture>(Picture.class));
    }
    
    public List<Picture> getPictures(){
        return jdbcTemplate.query("select pictures.*, albums.title from pictures JOIN albums ON albums.id = pictures.albumId",new RowMapper<Picture>(){
            public Picture mapRow(ResultSet rs, int row) throws SQLException {
                Picture e=new Picture();
                e.setId(rs.getInt(1));
                e.setTitle(rs.getString(2));
                e.setDescription(rs.getString(3));
                e.setTimeAdded(rs.getString(4));
                e.setAlbumId(rs.getInt(5));
                e.setPicture(rs.getBytes(6));
                e.setLikeCount(rs.getInt(7));
                e.setDislikeCount(rs.getInt(8));
                e.setAlbumName(rs.getString(9));
                return e;
            }
        });
    }
    public List<Picture> getFreshestPictures(){
        return jdbcTemplate.query("select\r\n" + 
        		"*\r\n" + 
        		"from pictures\r\n" + 
        		"JOIN albums\r\n" + 
        		"ON albums.id = pictures.albumId\r\n" + 
        		"ORDER BY\r\n" + 
        		"pictures.timeAdded DESC",new RowMapper<Picture>(){
            public Picture mapRow(ResultSet rs, int row) throws SQLException {
                Picture e=new Picture();
                e.setId(rs.getInt(1));
                e.setTitle(rs.getString(2));
                e.setDescription(rs.getString(3));
                e.setTimeAdded(rs.getString(4));
                e.setAlbumId(rs.getInt(5));
                e.setPicture(rs.getBytes(6));
                e.setLikeCount(rs.getInt(7));
                e.setDislikeCount(rs.getInt(8));  
                e.setAlbumName(rs.getString(10));
                e.setAlbumDescription(rs.getString(11));
                return e;
            }
        });
    }
    
    public List<Picture> getPicturesByAlbumId(int albumId){
        return jdbcTemplate.query("select\r\n" + 
        		"*\r\n" + 
        		"from pictures\r\n" + 
        		"JOIN albums\r\n" + 
        		"ON albums.id = pictures.albumId\r\n" + 
        		"WHERE\r\n" + 
        		"albumId =" + albumId ,new RowMapper<Picture>(){
            public Picture mapRow(ResultSet rs, int row) throws SQLException {
                Picture e=new Picture();
                e.setId(rs.getInt(1));
                e.setTitle(rs.getString(2));
                e.setDescription(rs.getString(3));
                e.setTimeAdded(rs.getString(4));
                e.setAlbumId(rs.getInt(5));
                e.setPicture(rs.getBytes(6));
                e.setLikeCount(rs.getInt(7));
                e.setDislikeCount(rs.getInt(8));
                e.setAlbumName(rs.getString(10));
                e.setAlbumDescription(rs.getString(11));
                return e;
            }
        });
    }
        		
        		
        		
        		
    public List<Picture> getMostPopularPictures(){
        return jdbcTemplate.query("select\r\n" + 
        		"*\r\n" + 
        		"from pictures\r\n" + 
        		"LEFT JOIN albums\r\n" + 
        		"ON albums.id = pictures.albumId\r\n" + 
        		"ORDER BY\r\n" + 
        		"pictures.likeCount DESC",new RowMapper<Picture>(){
            public Picture mapRow(ResultSet rs, int row) throws SQLException {
                Picture e=new Picture();
                e.setId(rs.getInt(1));
                e.setTitle(rs.getString(2));
                e.setDescription(rs.getString(3));
                e.setTimeAdded(rs.getString(4));
                e.setAlbumId(rs.getInt(5));
                e.setPicture(rs.getBytes(6));
                e.setLikeCount(rs.getInt(7));
                e.setDislikeCount(rs.getInt(8));
                e.setAlbumId(rs.getInt(9));
                e.setAlbumName(rs.getString(10));
                e.setAlbumDescription(rs.getString(11));
                return e;
            }
        });
    }
    
    
    
    public List<Album> getAlbumId(){  
        return jdbcTemplate.query("select id, title from albums",new RowMapper<Album>(){  
            public Album mapRow(ResultSet rs, int row) throws SQLException {  
                Album album=new Album();  
                album.setId(rs.getInt(1));  
                album.setTitle(rs.getString(2));  
                return album;  
            }  
        });  
      
    }
    
    public int getPictureId(List<Picture> list) {
    	
    	int id = ((Picture) list).getId();
    	return id;

    }


}