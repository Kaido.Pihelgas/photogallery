package com.hellokoding.account.repository;


import java.sql.ResultSet;  
import java.sql.SQLException;

import java.util.List;  
import org.springframework.jdbc.core.BeanPropertyRowMapper;  
import org.springframework.jdbc.core.JdbcTemplate;  
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;


import com.hellokoding.account.model.Album;
import com.hellokoding.account.model.AlbumPicture;
 
public class AlbumDao {
    private JdbcTemplate template;
    
    public void setTemplate(JdbcTemplate template) {
        this.template = template;
    }
    public int save(Album p){
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("title", p.getTitle());
        mapSqlParameterSource.addValue("description", p.getDescription());
       
           NamedParameterJdbcTemplate jdbcTemplateObject = new
                     NamedParameterJdbcTemplate(template.getDataSource());
           
        String sql = "insert into Albums(title,description) "
                + "VALUES (:title, :description)";

       return jdbcTemplateObject.update(sql, mapSqlParameterSource);
    }

    public int update(Album p){
        String sql="update Albums set title='"+p.getTitle()+"',description='"+p.getDescription()+"' where id="+p.getId()+"";
        return template.update(sql);
    }
    public int delete(int id){
        String sql="delete from Albums where id="+id+"";
        return template.update(sql);
    }
    public Album getAlbumById(int id){
        String sql="select * from Albums where id=?";
        return template.queryForObject(sql, new Object[]{id},new BeanPropertyRowMapper<Album>(Album.class));
    }
    
    public int getPreviousAlbumById(int id){
        String sql="select COALESCE(max(id), 0) from Albums where id<" + id;
        return template.queryForInt(sql);
    }
    
    public int getNextAlbumById(int id){
        String sql="select COALESCE(min(id), 0) from Albums where id>" + id;
        return  template.queryForInt(sql);
    }
    public List<Album> getAlbums(){
        return template.query("select * from Albums",new RowMapper<Album>(){
            public Album mapRow(ResultSet rs, int row) throws SQLException {
                Album e=new Album();
                e.setId(rs.getInt(1));
                e.setTitle(rs.getString(2));
                e.setDescription(rs.getString(3));
                return e;
            }
        });
    }
    public List<AlbumPicture> getAlbumsAndPicture(){
        return template.query("select a.title, p.id, a.id from  accounts.pictures as p join albums as a on a.id=p.albumId group by albumId",new RowMapper<AlbumPicture>(){
            public AlbumPicture mapRow(ResultSet rs, int row) throws SQLException {
            	AlbumPicture e=new AlbumPicture();
            	e.setAlbumName(rs.getString(1));
            	e.setPictureId(rs.getInt(2));
            	e.setAlbumId(rs.getInt(3));
            
                return e;
            }
        });
    }
} 

