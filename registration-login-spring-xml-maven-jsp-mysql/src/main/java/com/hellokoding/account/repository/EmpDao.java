package com.hellokoding.account.repository;  
import java.io.ByteArrayInputStream;
import java.sql.Types;
import java.sql.ResultSet;  
import java.sql.SQLException;  
import java.util.List;  
import org.springframework.jdbc.core.BeanPropertyRowMapper;  
import org.springframework.jdbc.core.JdbcTemplate;  
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.support.SqlLobValue;
import org.springframework.jdbc.support.lob.DefaultLobHandler;


import com.hellokoding.account.model.Emp;
import com.hellokoding.account.model.Picture;  
  
public class EmpDao {  
JdbcTemplate template;
  
public void setTemplate(JdbcTemplate template) {  
    this.template = template;  
}
public int save(Emp p){
    MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
    mapSqlParameterSource.addValue("name", p.getName());
    mapSqlParameterSource.addValue("salary", p.getSalary());
    mapSqlParameterSource.addValue("designation", p.getDesignation());
    mapSqlParameterSource.addValue("picture",  new SqlLobValue(new ByteArrayInputStream(p.getPicture()),
       p.getPicture().length, new DefaultLobHandler()), Types.BLOB);
       NamedParameterJdbcTemplate jdbcTemplateObject = new
                 NamedParameterJdbcTemplate(template.getDataSource());
       
    String sql = "insert into Emp99(name,salary,designation,picture,departmentId) "
            + "VALUES (:name, :salary, :designation, :picture)";

   return jdbcTemplateObject.update(sql, mapSqlParameterSource);
}

public int update(Emp p){  
    String sql="update Emp99 set name='"+p.getName()+"', salary="+p.getSalary()+",designation='"+p.getDesignation() +" where id="+p.getId()+"";  
    return template.update(sql);  
}  
public int delete(int id){  
    String sql="delete from Emp99 where id="+id+"";  
    return template.update(sql);  
}  
public Emp getEmpById(int id){  
    String sql="select * from Emp99 where id=?";  
    return template.queryForObject(sql, new Object[]{id},new BeanPropertyRowMapper<Emp>(Emp.class));  
}  
  
public List<Emp> getEmployees(){  
    return template.query("select * from Emp99",new RowMapper<Emp>(){  
        public Emp mapRow(ResultSet rs, int row) throws SQLException {  
            Emp e=new Emp();  
            e.setId(rs.getInt(1));  
            e.setName(rs.getString(2));  
            e.setSalary(rs.getFloat(3));  
            e.setDesignation(rs.getString(4));  
            return e;  
        }  
    });  
}

public List<Picture> getPictures(){
    return template.query("select * from pictures",new RowMapper<Picture>(){
        public Picture mapRow(ResultSet rs, int row) throws SQLException {
            Picture e=new Picture();
            e.setId(rs.getInt(1));
            e.setTitle(rs.getString(2));
            e.setDescription(rs.getString(3));
            e.setAlbumId(rs.getInt(4));
            e.setPicture(rs.getBytes(5));
            return e;
        }
    });
}

public int getPictureId(List<Picture> list) {
	
	int id = ((Picture) list).getId();
	return id;

}

public Picture getPictureById(int id){
    String sql="select * from pictures where id=?";
    return template.queryForObject(sql, new Object[]{id},new BeanPropertyRowMapper<Picture>(Picture.class));

}  

}  