package com.hellokoding.account.service;

import com.hellokoding.account.model.User;

public interface UserService {
    void save(User user);

    User findByUsername(String username);
    boolean hasRole(String roleName);

	User getLoggedInUser(); 
}
